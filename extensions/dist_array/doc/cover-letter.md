## Introduction

This document is a working group draft for the new UPC++ Extras dist_array
feature that is currently under development at LBNL and due to appear
in a forthcoming release of the [UPC++ Extras](https://bitbucket.org/berkeleylab/upcxx-extras) package.
This draft proposes an interface for a new `upcxx::extras::dist_array` class that
provides a shared distributed array abstraction, semantically similar to the
blocked shared arrays provided in the UPC language, but implemented
over UPC++ `dist_object`.


This document is a non-final working group draft and is intended for readers
who have some familiarity with C++ and the 
[UPC++ Specification, Revision 2019.9.0](https://bitbucket.org/berkeleylab/upcxx/downloads/upcxx-spec-2019.9.0.pdf). 
It is a being distributed to UPC++ stakeholders to solicit
feedback in the early stages of the design process. All of the extensions
described herein are subject to change without notice, 
and are expected to continue evolving in response to stakeholder comments.

### Providing Feedback

Readers are encouraged to provide feedback and comments on this working group
draft via one of the following channels:

1. Enter an issue in the UPC++ Issue Tracker 
   (linked from [https://upcxx-bugs.lbl.gov](https://upcxx-bugs.lbl.gov)). 
   Select the upcxx-extras component and please be sure to clearly identify the document revision.

2. Email to [upcxx-spec@googlegroups.com](mailto:upcxx-spec@googlegroups.com) - 
   this is a semi-private forum for people interested in the development and
   evolution of the UPC++ specification. 
   Discussions are not publicly visible, but interested parties are welcome to
   join upon request.  

3. Real-time video chat meetings with the Pagoda group's specification
   developers can be arranged upon request.


