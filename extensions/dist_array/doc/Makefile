# The goal of this Makefile is to support a decently formatted document for three viewers: plain
# html, pdf (via latex) and bitbucket. Supporting bitbucket is not that important, since we mainly
# expect this file to be viewed as a pdf or html.

# The most painful thing is figure captions. In plain html, they are specified in the image format
# ![caption text](imagefile), but in bitbucket, they are specified after the image file,
# i.e. ![ignored](imagefile "caption text"). And to complicate matters further, for conversion to
# latex with pandoc, it automatically labels and numbers the images with Figure X, whereas we need
# to add that for html/bitbucket. Consequently, we have a preprocessing step to remove the Figure X
# text for the pdf output.

title="UPC++ dist_array Working Group Draft"
revision="Revision 2020.3.0"
subtitle="https://upcxx.lbl.gov"

TARGET = dist_array
INPUT = copy-ack-legal.md cover-letter.md README.md

	#-createdate="2020:01:06 00:00:00" -modifydate="2020:01:06 00:00:00" 
pdfprops=-title=$(title)", "$(revision) \
	-subject=$(subtitle) \
	-author="J. Bachan, D. Bonachea, P. Hargrove, A. Kamil, D. Waters" \
	-keywords="UPC++, PGAS, GASNet, Exascale Computing, Working Group Draft, scientific computing, parallel distributed programming"

exiftool:=$(shell command -v exiftool 2> /dev/null)
pdflatex:=$(shell command -v pdflatex 2> /dev/null)

# styles that look reasonable when printed greyscale:
# haddock is preferred
# tango and kate look nice but consume more vertical space
# monochrome is too busy and ugly on monitors
# pygments (default) leaves comments too hard to read
pandocopts= --toc --number-sections --shift-heading-level-by=-1 --highlight-style=haddock

all: $(TARGET).pdf $(TARGET).html

.INTERMEDIATE: README.md

README.md: ../README.md
	grep -v -e '^# dist' -e '^#!C++' $^ > $@

    # embed-image replaces all images with inline base64 encodings so this is truly standalone
$(TARGET).html: $(INPUT)
	printf "<center><h1>%s<br>%s</h1><h3>%s</h3></center>\n<h2>Contents</h2>\n" $(title) $(revision) $(subtitle) > header.html
	pandoc --metadata pagetitle=$(title)", "$(revision) $(pandocopts) \
               pandoc.css -H header.html \
	       -o $@ $^
	#./embed-image.sh $@

$(TARGET).pdf: $(INPUT)
ifndef pdflatex
	$(warning WARNING: pdflatex is not availbale - cannot generate pdf)
else
	pandoc -V title=$(subst _,\_,$(title))'\\'$(revision)'\\[15pt]\large{}'$(subtitle) \
	       -V fontsize=10pt -V geometry:margin=1in -V linkcolor=blue \
	       $(pandocopts) \
	       -o $@ $^
ifndef exiftool
	$(warning WARNING: exiftool is not available - cannot add pdf metadata)
else
	exiftool $(pdfprops) $(TARGET).pdf
endif
endif

clean:
	rm -f $(TARGET).html $(TARGET).pdf *_original header.html *.base64

.PHONY: clean force
