#include <random>
#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char* argv[]) {
    if (argc>1) {
        std::cerr << "Usage: upcxx-run -n [RANKS] DA-cache" << std::endl;
        std::terminate();
    }
    init();
    // alloc array with 100 elements on each rank and a 2-way cache
    extras::dist_array<uint64_t> DA(100*rank_n(), extras::PURE_BLOCKED, world(), 
            extras::dist_array<uint64_t>::params(2));
    std::default_random_engine gen;
    std::uniform_int_distribution<uint64_t> dist(0,100);
    uint64_t sum = 0, lsum = 0, rsum = 0;
    for (auto &e : DA.process_view()) {
        e = dist(gen);
        sum += e;
    } // each rank should have same data
    intrank_t left = ( rank_me() + rank_n() - 1 ) % rank_n();
    intrank_t right = ( rank_me() + 1 ) % rank_n();
    size_t leftidx = DA.process_idx_info(0, left).global_idx();
    size_t rightidx = DA.process_idx_info(0, right).global_idx();
    barrier();
    future<> done = make_future();
    for (size_t i=0; i<DA.block_size(); i++) {
      done = when_all(done,
               DA.ptr(leftidx+i).then([&lsum](global_ptr<uint64_t> p) 
                   { return rget(p).then([&lsum](uint64_t d) { lsum += d; }); }),
               DA.ptr(rightidx+i).then([&rsum](global_ptr<uint64_t> p) 
                   { return rget(p).then([&rsum](uint64_t d) { rsum += d; }); })
             );
    }
    done.wait();
    upcxx_assert_msg(lsum==sum, "FAILURE: unexpected sum calculated for left neighbor");
    upcxx_assert_msg(sum==rsum, "FAILURE: unexpected sum calculated for right neighbor");
    barrier();
    if (!rank_me()) std::cout << "SUCCESS" << std::endl;
    finalize();
}
