#include "upcxx-extras/dist_array.hpp"

using namespace upcxx;

int main(int argc, char *argv[]) {
    if (argc>1) {
        std::cerr << "Usage: upcxx-run -n [RANKS] DA-alltoall" << std::endl;
        std::terminate();
    }
    init();
    // alloc as (default) pure blocked, rank_n() elements per rank
    extras::dist_array<double> my_dist_array(rank_n() * rank_n());  
    promise<> p;
    std::for_each(my_dist_array.bbegin(), my_dist_array.bend(),  // block iterators
                  [&p](future<global_ptr<double>> f_gptr) {
                    f_gptr.then([&p](global_ptr<double> gptr) { // base pointer to each rank
                       double val = rank_me()+1.;
                       rput(val, gptr + rank_me(), operation_cx::as_promise(p));
                    });
                  });
    p.finalize().wait();
    barrier();
    // every rank's segment should consist of the same value in each entry
    double entry = 0;
    for (auto &e : my_dist_array.process_view()) {
        if (++entry != e) {
            std::cerr << "ERROR: all-to-all ranks " 
                "sent disparate values" << std::endl;
            std::terminate();
        }
    }
    barrier();
    if (!rank_me()) std::cout << "SUCCESS" << std::endl;
    // ... consume data in my local mailboxes
    finalize();
}
