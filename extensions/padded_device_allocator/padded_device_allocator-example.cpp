#include <iostream>
#include <vector>

#include <upcxx/upcxx.hpp>
#include "upcxx-extras/padded_device_allocator.hpp"

using namespace std;
using namespace upcxx;
using namespace upcxx::extras;

int main() {
    upcxx::init();

    // alloc GPU segment
    size_t segsz = 1024 * 1024;
    #if UPCXX_VERSION >= 20210905
      using device_type = upcxx::gpu_default_device;
      auto gpu_alloc = make_gpu_allocator(segsz);
    #else
      using device_type = upcxx::cuda_device;
      device_type gpu_device( 0 );
      auto gpu_alloc = device_allocator<device_type>(gpu_device, segsz);
    #endif
    constexpr memory_kind device_kind = device_type::kind;

    // construct padded allocator utility
    auto pad_alloc = padded_device_allocator<device_type>(gpu_alloc);

    size_t width = 10;
    size_t height = 10;
    size_t pitch;
    global_ptr<double, device_kind> dptr =
        pad_alloc.allocate_pitched<double>(width, height, pitch);
    UPCXX_ASSERT_ALWAYS(dptr);

    std::cout << "Allocated device pointer with pitch " << pitch << std::endl;
    /*
     * Assert some loose but reasonable assumptions about pitch, given that most
     * CUDA GPUs have at least 128-byte memory transactions.
     */
    UPCXX_ASSERT_ALWAYS(pitch >= 128 && pitch % 128 == 0);

    // Show we are still able to use the flat CUDA allocation routines
    global_ptr<double, device_kind> dptr2 = pad_alloc.allocate<double>(width);
    UPCXX_ASSERT_ALWAYS(dptr2);

    std::vector<double> h_row(width);
    for (size_t i = 0; i < width; i++) {
        h_row[i] = i;
    }

    // Copy a single row from the host into the second row on the device
    global_ptr<double, device_kind> d_row = index_pitched(dptr, 1UL, 0UL, pitch);
    upcxx::copy(h_row.data(), d_row, width).wait();

    // clear host copy
    h_row.assign(width, -1);

    // Copy back from device to host
    upcxx::copy(d_row, h_row.data(), width).wait();

    for (size_t i = 0; i < width; i++) {
        UPCXX_ASSERT_ALWAYS(h_row[i] == i);
    }

    pad_alloc.deallocate(dptr2);
    pad_alloc.deallocate(dptr);

    #if UPCXX_VERSION >= 20210905
      gpu_alloc.destroy();
    #else
      gpu_device.destroy();
    #endif

    // For CI
    upcxx::barrier();
    if (upcxx::rank_me() == 0) std::cout << "SUCCESS" << std::endl;

    upcxx::finalize();

    return 0;
}
