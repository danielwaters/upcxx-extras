# Pitched UPC++ Device Allocator #

This header wraps a previously-constructed UPC++ device allocator in a padded 
device allocator class. If the device's pitch is not specified, it will be 
determined during construction. Unlike `device_allocator<Device>`, constructing
a `padded_device_allocator<Device>` is a non-collective operation.

```C++
    template<typename Device>
    upcxx::extras::padded_device_allocator(
      upcxx::device_allocator<Device> &device_allocator, std::size_t pitch = 0);
```
While most of the member functions simply call the corresponding function in the
enclosed device allocator, it additionally offers an `allocate_pitched` member function:

```C++
    template<typename T, upcxx::memory_kind kind>
    upcxx::global_ptr<T, kind> allocate_pitched(
            std::size_t width, std::size_t height, std::size_t &pitch);
```

when the allocation is on a CUDA device, this is analogous to [cudaMallocPitch](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html).

That is, this API allocates at least `width * height` elements of type `T` of linear
memory on the device. This function may pad the allocation to ensure that
corresponding pointers in any given row will continue to meet the alignment
requirements for coalescing as the address is updated from row to row.

On success, a `upcxx::global_ptr` pointing to the allocated memory is returned and
the out argument `pitch` is set to the pitch of the allocation in bytes. On
failure, a null global pointer is returned and the state of `pitch` is unspecified.

Note that this API differs from `cudaMallocPitch` in the units of allocation. In
`cudaMallocPitch`, the width and height parameters are expressed in terms of
*bytes*. In `allocate_pitched`, they are expressed in terms of *elements* of type `T`.

However, the value returned in the `pitch` argument of `allocate_pitched` is in
*bytes*. This is consistent with `cudaMallocPitch`. On a given device, if two calls
to `allocate_pitched` pass the same element type, width, and height they are guaranteed to
return the same pitch.

As an example, to allocate a 10 by 10 row-major matrix on a CUDA-capable GPU 
with each row aligned for performance would require the following. Note that 
this may add padding to the end of each row.

```C++
    size_t pitch;
    size_t width = 10;
    size_t height = 10;
    auto mat = padded_dev_alloc.allocate_pitched<double>(width, height, pitch);
```

Getting a pointer to the element at coordinate (row=4, col=3) in the above matrix can be
done using the provided `index_pitched` utility:

```C++
    auto ptr = upcxx::extras::index_pitched(mat, 4, 3, pitch);
```

Two variants of `index_pitched` are provided: one that operates on raw pointers
and another that operates on `upcxx::global_ptr`. The former can be used in both
host and device code, but the latter is only callable from host code. See below
for their function signatures. Note that they are identical except for the input
pointer type and the return type.

```C++
    template <typename T, typename index_type>
    T* index_pitched(T *ptr, index_type row, index_type col, index_type pitch);

    template <typename T, typename index_type, upcxx::memory_kind kind>
    upcxx::global_ptr<T, kind> index_pitched(upcxx::global_ptr<T, kind> ptr,
            index_type row, index_type col, index_type pitch);
```

In addition, the `byte_offset_pitched` API can be used to calculate the offset in
bytes of a given (row, col) coordinates in an array, given its allocation pitch
in bytes. Its signature is shown below. Like `index_pitched`, `byte_offset_pitched`
can be used in both host and device code.

```C++
    template <typename T, typename index_type>
    index_type byte_offset_pitched(index_type row, index_type col,
            index_type pitch);
```

`byte_offset_pitched` may be useful for kernels that are computing the offset of
some coordinates in several arrays, all of which have the same pitch.
`byte_offset_pitched` can be used a single time to compute that offset, and then
pointer arithmetic can be used to get a pointer in to each array:

```C++
    size_t offset = byte_offset_pitched<double>(i, j, pitch_bytes);
    double *A_i_j = (double *)((char *)A + offset);
    double *B_i_j = (double *)((char *)B + offset);
```

## Mixed Compilation ##

All of the above functionality is accessible from host code
(`padded_device_allocator`, `allocate_pitched`, `index_pitched`, etc).

However, when compiling a source file for a GPU device most of the above functionality
is not accessible. The only callable utility is the `T*` variant of `index_pitched`.

## Using `allocate_pitched` for 3D arrays ##

Allocating an aligned 3D array (depth x height x width) is also possible using
the height parameter of `allocate_pitched`:

```C++
    size_t pitch;
    size_t width = 10;
    size_t height = 10;
    size_t depth = 10;
    auto mat3d = padded_gpu_alloc.allocate_pitched<double>(width, height * depth, pitch);
```

`index_pitched` can still be used to calculate pointers into the matrix:


```C++
    auto ptr = upcxx::extras::index_pitched(mat, d * height + h, w, pitch);
```

A full example of this extension is provided in: 
[padded_device_allocator-example.cpp](padded_device_allocator-example.cpp).
