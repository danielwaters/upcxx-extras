This directory contains an example illustrating how to efficiently quiesce all
outstanding rpc_ff across a UPC++ job. This is useful in that rpc_ff does not
incur a roundtrip on the network, and is therefore better for the overall
performance of your app than rpc if your app does not require waiting on
individual RPC. However, correctly and efficiently waiting for
all outstanding rpc_ff can be tricky and so this example offers one approach.

This implementation depends on a working UPC++ v1.0 implementation, with the
upcxx compiler in the user's PATH or UPCXX_INSTALL set to the installation
directory.

This example can be compiled using the provided Makefile, though some
system-specific changes may be necessary:

    make
