#include <upcxx/upcxx.hpp>

#include <stdio.h>
#include <assert.h>

/*
 * rpc_ff_tracker is offered as an example/utility for tracking and waiting on
 * asynchronous, RPC operations that do not return a completion event to their
 * source that can be used to wait for completion. The most common example of
 * such an operation is a fire-and-forget RPC (rpc_ff), though you could also
 * use this class to track the completion of any RPC.
 *
 * At a high level, this class works by recording the number of
 * rpc_ff sent at every source and the number of rpc_ff executed at every
 * destination. A global reduction is then used to check and wait until the
 * number of sent rpc_ff is the same as the number of executed rpc_ff. It is the
 * responsibility of the user of this class to correctly use the record_send API
 * to record the injection of an rpc_ff on the sender, and use the
 * record_arrival API to record the execution of an rpc_ff at the destination.
 *
 * In the rpc_ff_tracker, a 'phase' is a grouping of rpc_ff that will all be
 * quiesced when wait_for_quiescence is called. When recording a send or an
 * arrival, we have to indicate which phase it counts against so that we can
 * correctly quiesce all of the operations in that phase. The phase passed to a
 * record_send/record_arrival pair must be the same for a given rpc_ff being
 * tracked by this class.
 */
class rpc_ff_tracker {
    public:
        rpc_ff_tracker() : current_phase(0) { }

        /*
         * This function, called with the current phase, records 1 or more sends
         * to be included in the set of operations for that phase. record_send
         * may be called prior to wait_for_quiescence for a given phase, or from
         * an rpc_ff inside progress during wait_for_quiescence (in the case of
         * nested rpc_ff calls).
         */
        void record_send(int phase, std::uint64_t count = 1) {
            assert(phase == 0 || phase == 1);
            sent_ops[phase] += count;
        }

        /*
         * This function, called with the phase of the send, records 1 or more receives.
         * Care must be taken to use the phase of the send, not the return from
         * get_phase(), to avoid a race with wait_for_quiescence(). In other
         * words, the RPC message should include a copy of the sender's current
         * phase (either passed as an explicit argument or embedded in a lambda
         * capture) which is then used to record_arrival().
         */
        void record_arrival(int phase, std::uint64_t count = 1) {
            assert(phase == 0 || phase == 1);
            executed_ops[phase] += count;
        }

        /*
         * This function waits for the send and receive counts to balance globally.
         * Makes user-level progress until the counts match.
         * Advances phase prior to return.
         */
        void wait_for_quiescence() {
            /*
             * Loop while not all rpc_ff have completed. Note that wait() enters
             * the progress engine, giving pending rpc_ff a chance to run on
             * this rank.
             */
            bool done;
            do {
                std::uint64_t local[2] = {sent_ops[current_phase], executed_ops[current_phase]};
                std::uint64_t global[2];
                upcxx::reduce_all(local, global, 2, upcxx::op_fast_add).wait();
                assert(global[0] >= global[1]);
                done = (global[0] == global[1]);
            } while (!done);

            // Reset state for the next phase
            executed_ops[current_phase] = 0;
            sent_ops[current_phase] = 0;
            current_phase = !current_phase;
        }

        /*
         * Fetch the current phase for this tracker on the current rank. The
         * most common usage for this API would be to pass the current phase to
         * record_send.
         */
        int get_phase() {
            return current_phase;
        }

    private:
        int current_phase;
        std::uint64_t executed_ops[2] = {0, 0};
        std::uint64_t sent_ops[2] = {0, 0};
};

/*
 * An example illustrating how to efficiently quiesce all outstanding rpc_ff
 * across a UPC++ job. This is useful in that rpc_ff does not incur a roundtrip
 * on the network, and is therefore better for the overall performance of your
 * app. However, correctly and efficiently waiting for all outstanding rpc_ff
 * can be tricky and so this example offers one approach. A simple
 * upcxx::barrier is insufficient, as it does not guarantee that all previous
 * RPCs have completed -- it only guarantees that all ranks have reached that
 * point in program execution (i.e. started all preceding rpc_ff) before any
 * advance past. It does not make any guarantees on the completion of preceding
 * rpc_ff.
 *
 * This example allocates a shared array on each rank of length l_N, and then
 * sends rpc_ff from each rank to random target ranks to perform random
 * increments in the shared array.
 */
int main(int argc, char **argv) {
    upcxx::init();

    const int nranks = upcxx::rank_n();
    const int me = upcxx::rank_me();

    int rounds = 10;
    int l_N = 1024;
    int n_ops = 4096;
    if (argc >= 2) rounds = atoi(argv[1]);
    if (argc >= 3) l_N = atoi(argv[2]);
    if (argc >= 4) n_ops = atoi(argv[3]);

    srand(me);

    upcxx::global_ptr<int> counts = upcxx::new_array<int>(l_N);
    assert(counts);
    memset(counts.local(), 0x00, l_N * sizeof(int));

    upcxx::dist_object<upcxx::global_ptr<int>> dobj(counts);

    // Object used to track the number of rpc_ff sent and completed on each rank.
    rpc_ff_tracker _tracker;
    upcxx::dist_object<rpc_ff_tracker> tracker(_tracker);

    /*
     * We execute multiple rounds to illustrate what state needs to be reset
     * after quiesence before entering a new round of issuing remote operations.
     */
    for (int r = 0; r < rounds; r++) {
        int phase = tracker->get_phase();

        for (int i = 0; i < n_ops; i++) {
            int target_rank = rand() % nranks;
            int offset = rand() % l_N;
            upcxx::rpc_ff(target_rank,
                    [offset, phase, l_N] (upcxx::dist_object<upcxx::global_ptr<int>>& counts,
                            upcxx::dist_object<rpc_ff_tracker>& tracker) {
                        (*counts).local()[offset] += 1;

                        // Send a nested rpc_ff that is also recorded in this phase.
                        int nested_target_rank = rand() % upcxx::rank_n();
                        int nested_offset = rand() % l_N;

                        upcxx::rpc_ff(nested_target_rank,
                            [nested_offset, phase] (upcxx::dist_object<upcxx::global_ptr<int>>& counts,
                                    upcxx::dist_object<rpc_ff_tracker>& tracker) {
                                (*counts).local()[nested_offset] += 1;
                                // Record the nested arrival
                                tracker->record_arrival(phase);
                            }, counts, tracker);
                        // Record the nested send
                        tracker->record_send(phase);

                        // Record the top-level arrival
                        tracker->record_arrival(phase);
                    }, dobj, tracker);
          
            // Record the top-level send
            tracker->record_send(phase);

            upcxx::progress();
        }

        /*
         * Wait for all rpc_ff that were recorded as sent in the current phase
         * to record arrival at their target rank. This call is collective
         * and offers barrier-like synchronization. Note that tracking rpc_ff
         * calls in a phase relies on accurate bookkeeping by the programmer
         * through matching calls to record_send on the sender and
         * record_arrival on the target.
         */
        tracker->wait_for_quiescence();
    }

    // Validation
    int sum = 0;
    for (int i = 0; i < l_N; i++) {
        sum += counts.local()[i];
    }
    sum = upcxx::reduce_all<int>(sum, upcxx::op_fast_add).wait();
    /*
     * We expect to receive 2 rpc_ff (1 top-level and 1 nested) for each op
     * issued by each rank in each round.
     */
    assert(sum == 2 * rounds * n_ops * nranks);

    upcxx::delete_array(counts);

    if (me == 0) {
        std::cout << "SUCCESS" << std::endl;
    }

    upcxx::finalize();

    return 0;
}
