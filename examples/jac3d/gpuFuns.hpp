#ifdef _GPUFUNS_H
#else
#define _GPUFUNS_H
__global__ void GPU_jac3d(int by, int Nx, int Ny, int Nz, double *d_U,
        double *d_Un, double *d_b); 
void gather_GPU(double* destination, double* source, int n_elements,int length,
        int displacement);
void scatter_GPU (double* destination, double* source, int n_elements,int  length,
        int displacement);
#endif
