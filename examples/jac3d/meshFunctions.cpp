#include <iostream>
#include <string.h>
#include <sstream>
#include <assert.h>
#include <math.h>
#include "config.hpp"

#include <upcxx/upcxx.hpp>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

using namespace upcxx;
using namespace upcxx::extras;
using namespace std;

void OrderlyOutput(ostringstream& s1);

// Turn this into a macro?
inline int index(int k, int j, int i, int nz, int ny, int nx) {
      return k*nx*ny + j *nx + i;
}

/****************************************************************
* Return the l2 norm of the residual over these Grids          *
*****************************************************************/
double  l2norm(double* U, double* B,const int nx, const int ny, const int nz,
        const int Nx, const int Ny, const int Nz){
    double err = 0;
    double h = 1.0, c2 = 1/ (h * h);

    for (int k=1; k<nz-1; k++)
        for (int j=1; j<ny-1; j++)
            for (int i=1; i<nx-1; i++){
                double du = c2 * (U[index(k,   j,   i-1, nz, ny, nx)] +
                                  U[index(k,   j,   i+1, nz, ny, nx)] +
                                  U[index(k,   j-1, i,   nz, ny, nx)] +
                                  U[index(k,   j+1, i,   nz, ny, nx)] +
                                  U[index(k-1, j,   i,   nz, ny, nx)] +
                                  U[index(k+1, j,   i,   nz, ny, nx)] -
                              6.0*U[index(k,   j,   i,   nz, ny, nx)]);

                double r = B[index(k, j, i, nz, ny, nx)] - du;
                err = err +  r*r;
            }

    double global_err = upcxx::reduce_one(err, upcxx::op_fast_add, 0).wait();
    return sqrt(global_err/((double)((Nx+1)*(Ny+1)*(Nz+1))));
}

void initMesh(int *n, int *p, int *r, double *U, double *Un, double *B) {
    double c = (1.0-M_PI*M_PI/4)*3.0*M_PI*M_PI;

    for (int k=0; k<n[2]+2; k++) {
        for (int j=0; j<n[1]+2; j++) {
            for (int i=0; i<n[0]+2; i++) {
                int ind = i + j*(n[0]+2) + k*(n[0]+2)*(n[1]+2);
                Un[ind] = 1.0; U[ind] = 1.0;

#ifdef POISSON
                if (k == 0 || k == n[2] + 1 || j == 0 || j == n[1] + 1 ||
                        i == 0 || i == n[0] + 1) {
                    B[ind] = 0;
                } else {
                    int global_i = r[0] * n[0] + (i - 1);
                    int global_j = r[1] * n[1] + (j - 1);
                    int global_k = r[2] * n[2] + (k - 1);

                    B[ind] = 6.*c*sin(M_PI*global_i)*sin(M_PI*global_j)*
                        sin(M_PI*global_k);
                }
#endif
                if (i==0) {
                    if(r[0]==0){
                        Un[ind] = 0.0; U[ind] = 0.0;
                    }
                } else
                    if (i==n[0]+1) {
                        if(r[0]==p[0]-1){
                            Un[ind] = 0.0; U[ind] = 0.0;
                        }
                    }
                if (j==0) {
                    if (r[1]==0){ 
                        Un[ind] = 0.0; U[ind] = 0.0;
                    }
                } else if(j==n[1]+1){
                    if (r[1]==p[1]-1){
                        Un[ind] = 0.0; U[ind] = 0.0;
                    }
                }
                if (k==0) {
                    if (r[2]==0) {
                        Un[ind] = 0.0; U[ind] = 0.0; 
                    }
                }
                else if (k==n[2]+1) {
                    if (r[2]==p[2]-1) {
                        Un[ind] = 0.0; U[ind] = 0.0;  
                    }
                }
            }
        }
    }
}
