#include <cuda.h>
#include "config.hpp"

#define DIM_BLOCK 512

__global__ void gather_GPU(double* destination, double* source, int n_elements, int displacement){
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid < n_elements) {
        int src_idx = tid * displacement;
        int dst_idx = tid;
        destination[dst_idx] = source[src_idx];
    }
}

__global__ void scatter_GPU(double* destination, double* source, int n_elements,int displacement){
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid < n_elements) {
        int src_idx = tid;
        int dst_idx = tid * displacement;
        destination[dst_idx] = source[src_idx];
    }
}

void gather_GPU(double* destination, double* source, int n_elements,int length,
        int displacement){  //length in units

    /*
     * This function transfers several contiguous blocks of elements from the
     * source to the destination array.
     *
     * n_elements indicates how many contiguous blocks to transfer.
     *
     * length indicates the number of elements in each block.
     *
     * displacement indicates the stride of the start offset of each block.
     *
     * When copying one element at a time, it is more efficient to use a GPU
     * kernel to perform this work in parallel (though bandwidth utilization
     * will likely still be low). Otherwise, we use cudaMemcpy to efficiently
     * gather multiple elements at once.
     */

    if (length==1) {
        gather_GPU<<<(n_elements + DIM_BLOCK - 1) / DIM_BLOCK, DIM_BLOCK>>>(
                destination, source, n_elements, displacement);
    } else {
        int offset =0;
        for (int i=0; i< n_elements; i++) {
            cudaMemcpy(&destination[i*length], &source[offset],
                    length*sizeof(double),cudaMemcpyDeviceToDevice);
            offset += displacement;
        }
    }
}

void scatter_GPU(double* destination, double* source, int n_elements,
        int  length,int displacement){
  if(length==1){
    scatter_GPU<<<(n_elements + DIM_BLOCK - 1) / DIM_BLOCK, DIM_BLOCK>>>(
            destination, source, n_elements, displacement);
  }else{
    int offset = 0;
    for(int i=0; i< n_elements; i++){
        cudaMemcpy(&destination[offset], &source[i*length],
                length*sizeof(double), cudaMemcpyDeviceToDevice);
        offset += displacement;
    }
  }
}
