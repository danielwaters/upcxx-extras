This directory contains an example of Cannon's algorithm written in UPC++.

Cannon's algorithm is a scalable parallel implementation of matrix-matrix
multiplication which exchanges more computation for less communication than a
naive, classical implementation might use. Details on Cannon's algorithm can be
found on its Wikipedia page (https://en.wikipedia.org/wiki/Cannon%27s_algorithm).

This implementation depends on:

  - A working UPC++ v1.0 implementation (2019.3.0 or later), with the upcxx
    compiler in the user's PATH
  - A BLAS installation with "cblas.h" header, with the environment variables
    BLAS_INSTALL and CBLAS_INSTALL set appropriately.

The provided Makefile tries to find usable BLAS installations, which on most
platforms requires a working "cblas.h" header and supporting libraries. Many
platforms should work without changes or by setting BLAS_INSTALL to point to
the root installation prefix for the BLAS library (which may be "/usr"). If
that doesn't work, then BLAS_HEADER_DIRS and BLAS_LIBS can be set to the
proper include and library compiler options, respectively. If your system
lacks a working BLAS library, the [OpenBLAS](https://www.openblas.net/) library
might be a good, free choice. Note that cannon's computational performance is
heavily dependent on a good BLAS library but we do not endorse the performance
of any particular BLAS implementation.

This example can be optionally build to use CUDA devices (with compute
capability 3.0 or higher) to accelerate the computation in Cannon's algorithm
by offloading the GEMM kernel to the CUBLAS library. An example Makefile rule
(cannon_cuda) is included in the provided Makefile. This version of the code
leverages UPC++'s memory kinds support to perform direct copies between CUDA
devices on different nodes using upcxx::copy, and therefore requires a minimum
of UPC++ v1.0 2019.3.0.

By default, this Makefile will compile CUDA kernels without specifying any GPU
code generation options, allowing nvcc's default to be used.  To specify a
single real or virtual architecture, set the NVCCARCH environment variable prior
to calling make. For instance, to generate code for the V100 GPUs on Summit:

    env NVCCARCH=sm_70 make

For more complex cases, one may instead set NVCCARCH_FLAGS:

    env NVCCARCH_FLAGS='-Wno-deprecated-gpu-targets -gencode arch=compute_35,code=compute_35' make
