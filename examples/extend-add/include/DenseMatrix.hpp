/*
 * Portions of the code in this file are adapted from STRUMPACK, see strumpack/license.txt
 *
 */
#ifndef DENSE_MATRIX_HPP
#define DENSE_MATRIX_HPP

#include <string>
#include <iomanip>
#include <cassert>
#include <algorithm>

#if defined(_OPENMP)
#include <omp.h>
#endif

#include "Utils.hpp"
#include "Random_wrapper.hpp"

namespace strumpack {

  template<typename scalar_t> class DistributedMatrix;

  template<typename scalar_t> class DenseMatrix {
    using real_t = typename RealType<scalar_t>::value_type;

  protected:
    scalar_t* _data;
    std::size_t _rows;
    std::size_t _cols;
    std::size_t _ld;

  public:
    DenseMatrix();
    DenseMatrix(std::size_t m, std::size_t n);
    DenseMatrix
    (std::size_t m, std::size_t n, const scalar_t* D, std::size_t ld);
    DenseMatrix
    (std::size_t m, std::size_t n, const DenseMatrix<scalar_t>& D,
     std::size_t i, std::size_t j);
    DenseMatrix(const DenseMatrix<scalar_t>& D);
    DenseMatrix(DenseMatrix<scalar_t>&& D);
    virtual ~DenseMatrix();

    DenseMatrix<scalar_t>& operator=(const DenseMatrix<scalar_t>& D);
    DenseMatrix<scalar_t>& operator=(DenseMatrix<scalar_t>&& D);

    inline std::size_t rows() const { return _rows; }
    inline std::size_t cols() const { return _cols; }
    inline std::size_t ld() const { return _ld; }
    inline const scalar_t* data() const { return _data; }
    inline scalar_t* data() { return _data; }
    inline const scalar_t& operator()(std::size_t i, std::size_t j) const
    { assert(i<rows() && j<cols()); return _data[i+_ld*j]; }
    inline const scalar_t* ptr(std::size_t i, std::size_t j) const
    { assert(i<rows() && j<cols()); return _data+i+_ld*j; }
    inline scalar_t& operator()(std::size_t i, std::size_t j)
    { assert(i<rows() && j<cols()); return _data[i+_ld*j]; }
    inline scalar_t* ptr(std::size_t i, std::size_t j)
    { assert(i<rows() && j<cols()); return _data+i+_ld*j; }

    void random();
    void random
    (random::RandomGeneratorBase<typename RealType<scalar_t>::
     value_type>& rgen);
    void fill(scalar_t v);
    void zero();
    void eye();
    virtual void clear();
    void resize(std::size_t m, std::size_t n);
    void copy(const scalar_t* B, std::size_t ldb);

    virtual std::size_t memory() const {
      return sizeof(scalar_t) * rows() * cols();
    }
    virtual std::size_t nonzeros() const {
      return rows()*cols();
    }

    std::vector<int> LU(int depth);

  private:
    template<typename T> friend class DistributedMatrix;
  };


  template<typename scalar_t> DenseMatrix<scalar_t>
  vconcat(const DenseMatrix<scalar_t>& a, const DenseMatrix<scalar_t>& b) {
    assert(a.cols() == b.cols());
    DenseMatrix<scalar_t> tmp(a.rows()+b.rows(), a.cols());
    copy(a, tmp, 0, 0);
    copy(b, tmp, a.rows(), 0);
    return tmp;
  }

  template<typename scalar_t> DenseMatrix<scalar_t>
  hconcat(const DenseMatrix<scalar_t>& a, const DenseMatrix<scalar_t>& b) {
    assert(a.rows() == b.rows());
    DenseMatrix<scalar_t> tmp(a.rows(), a.cols()+b.cols());
    copy(a, tmp, 0, 0);
    copy(b, tmp, 0, a.cols());
    return tmp;
  }

  template<typename scalar_t> DenseMatrix<scalar_t>
  eye(std::size_t m, std::size_t n) {
    DenseMatrix<scalar_t> I(m, n);
    I.eye();
    return I;
  }


  template<typename scalar_t> DenseMatrix<scalar_t>::DenseMatrix()
    : _data(nullptr), _rows(0), _cols(0), _ld(1) { }

  template<typename scalar_t> DenseMatrix<scalar_t>::DenseMatrix
  (std::size_t m, std::size_t n)
    : _data(new scalar_t[m*n]), _rows(m),
      _cols(n), _ld(std::max(std::size_t(1), m)) { }

  template<typename scalar_t> DenseMatrix<scalar_t>::DenseMatrix
  (std::size_t m, std::size_t n, const scalar_t* D, std::size_t ld)
    : _data(new scalar_t[m*n]), _rows(m), _cols(n),
      _ld(std::max(std::size_t(1), m)) {
    assert(ld >= m);
    for (std::size_t j=0; j<_cols; j++)
      for (std::size_t i=0; i<_rows; i++)
        operator()(i, j) = D[i+j*ld];
  }

  template<typename scalar_t> DenseMatrix<scalar_t>::DenseMatrix
  (std::size_t m, std::size_t n, const DenseMatrix<scalar_t>& D,
   std::size_t i, std::size_t j)
    : _data(new scalar_t[m*n]), _rows(m), _cols(n),
      _ld(std::max(std::size_t(1), m)) {
    for (std::size_t _j=0; _j<std::min(_cols, D.cols()-j); _j++)
      for (std::size_t _i=0; _i<std::min(_rows, D.rows()-i); _i++)
        operator()(_i, _j) = D(_i+i, _j+j);
  }

  template<typename scalar_t>
  DenseMatrix<scalar_t>::DenseMatrix(const DenseMatrix<scalar_t>& D)
    : _data(new scalar_t[D.rows()*D.cols()]), _rows(D.rows()),
      _cols(D.cols()), _ld(std::max(std::size_t(1), D.rows())) {
    for (std::size_t j=0; j<_cols; j++)
      for (std::size_t i=0; i<_rows; i++)
        operator()(i, j) = D(i, j);
  }

  template<typename scalar_t>
  DenseMatrix<scalar_t>::DenseMatrix(DenseMatrix<scalar_t>&& D)
    : _data(D.data()), _rows(D.rows()), _cols(D.cols()), _ld(D.ld()) {
    D._data = nullptr;
    D._rows = 0;
    D._cols = 0;
    D._ld = 1;
  }

  template<typename scalar_t> DenseMatrix<scalar_t>::~DenseMatrix() {
    delete[] _data;
  }

  template<typename scalar_t> DenseMatrix<scalar_t>&
  DenseMatrix<scalar_t>::operator=(const DenseMatrix<scalar_t>& D) {
    if (this == &D) return *this;
    if (_rows != D.rows() || _cols != D.cols()) {
      _rows = D.rows();
      _cols = D.cols();
      delete[] _data;
      _data = new scalar_t[_rows*_cols];
      _ld = std::max(std::size_t(1), _rows);
    }
    for (std::size_t j=0; j<_cols; j++)
      for (std::size_t i=0; i<_rows; i++)
        operator()(i,j) = D(i,j);
    return *this;
  }

  template<typename scalar_t> DenseMatrix<scalar_t>&
  DenseMatrix<scalar_t>::operator=(DenseMatrix<scalar_t>&& D) {
    _rows = D.rows();
    _cols = D.cols();
    _ld = D.ld();
    delete[] _data;
    _data = D.data();
    D._data = nullptr;
    return *this;
  }

  template<typename scalar_t> void
  DenseMatrix<scalar_t>::random
  (random::RandomGeneratorBase<typename RealType<scalar_t>::
   value_type>& rgen) {
    for (std::size_t j=0; j<cols(); j++)
      for (std::size_t i=0; i<rows(); i++)
        operator()(i,j) = rgen.get();
  }

  template<typename scalar_t> void DenseMatrix<scalar_t>::random() {
    auto rgen = random::make_default_random_generator<real_t>();
    for (std::size_t j=0; j<cols(); j++)
      for (std::size_t i=0; i<rows(); i++)
        operator()(i,j) = rgen->get();
  }

  template<typename scalar_t> void DenseMatrix<scalar_t>::zero() {
    for (std::size_t j=0; j<cols(); j++)
      for (std::size_t i=0; i<rows(); i++)
        operator()(i,j) = scalar_t(0.);
  }

  template<typename scalar_t> void DenseMatrix<scalar_t>::fill(scalar_t v) {
    for (std::size_t j=0; j<cols(); j++)
      for (std::size_t i=0; i<rows(); i++)
        operator()(i,j) = v;
  }

  template<typename scalar_t> void DenseMatrix<scalar_t>::eye() {
    auto minmn = std::min(cols(), rows());
    for (std::size_t j=0; j<minmn; j++)
      for (std::size_t i=0; i<minmn; i++)
        operator()(i,j) = (i == j) ? scalar_t(1.) : scalar_t(0.);
  }

  template<typename scalar_t> void DenseMatrix<scalar_t>::clear() {
    _rows = 0;
    _cols = 0;
    _ld = 1;
    delete[] _data;
    _data = nullptr;
  }

  template<typename scalar_t> void
  DenseMatrix<scalar_t>::resize(std::size_t m, std::size_t n) {
    auto tmp = new scalar_t[m*n];
    for (std::size_t j=0; j<std::min(cols(),n); j++)
      for (std::size_t i=0; i<std::min(rows(),m); i++)
        tmp[i+j*m] = operator()(i,j);
    delete[] _data;
    _data = tmp;
    _ld = std::max(std::size_t(1), m);
    _rows = m;
    _cols = n;
  }

  template<typename scalar_t> void
  DenseMatrix<scalar_t>::copy(const scalar_t* B, std::size_t ldb) {
    assert(ldb >= rows());
    for (std::size_t j=0; j<cols(); j++)
      for (std::size_t i=0; i<rows(); i++)
        operator()(i,j) = B[i+j*ldb];
  }

  template<typename scalar_t> std::vector<int>
  DenseMatrix<scalar_t>::LU(int depth) {
    std::vector<int> piv(rows());
    return piv;
  }

} // end namespace strumpack

#endif // DENSE_MATRIX_HPP
