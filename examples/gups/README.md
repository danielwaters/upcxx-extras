# HPC Challenge RandomAccess #

This directory contains various implementations of the 
HPC Challenge RandomAccess benchmark (aka "gups").

Except where otherwise noted, all files in this directory are 
authored by Scott B. Baden and Dan Bonachea, and are subject to the copyright
notice and licensing terms in the top-level [LICENSE.txt](../../LICENSE.txt).

The UPC++ implementations require UPC++ 1.0 v2018.9.0 or later,
which is available [here](https://upcxx.lbl.gov).
The AMO version requires a bug fix introduced in 2018.9.3.

For benchmark specification and rules, see:
http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/

# DO NOT COMPARE PERFORMANCE OF THESE IMPLEMENTATIONS WITH VERSIONS THAT AGGREGATE UPDATES!!!

IMPORTANT NOTE: The UPC++ RandomAccess benchmark implementations currently present in
this directory deliberately employ FINE-GRAINED algorithms to perform table updates.
This means that every table update corresponds to one or more independent
low-level communication operations (in the case of an update to a remote table entry).
The benchmarks are written this way to stress and reveal the fine-grained overheads
associated with the communication system. These implementations are NOT
intended to produce results that are comparable with alternate implementations
that aggregate updates, ie packing several updates into a single communication
operation to amortize network-level overheads.

