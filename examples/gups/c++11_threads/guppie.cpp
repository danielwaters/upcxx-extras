// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   C++ version of gups based on the upc version of guppie [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//

#define __STDC_FORMAT_MACROS 1
#include <stdint.h>

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>
#include <thread>
#include <chrono>

#include "CTBarrier.h"
#include "block.cpp"
#include "startr.cpp"
#include "cmdLine.cpp"
#include "guppie.hpp"

using namespace std;

CombiningTreeBarrier *barr;

// Allocate main table
uint64 *table;

void reportResults(double walltime_i, double walltime, int64 nupdate, int ltabsize, uint64 totalErrors, double thresh, int vlen, int NT);
//
//  update_table -- multi-threaded update_table for 2 or more threads
//
void update_table(int TID, int NT, int Vlen,
                  int   ltabsize,
		  const int64 tabsize, const int64 nupdate, double thresh)
{
    uint64 nerrors;
    uint64 *local_table;
    int64 i, j;
    int64 i0, i1, locTabSize;
    Block(TID, NT, tabsize, i0, i1, locTabSize);

    barr->bsync(TID);

    // Begin init timing here
    auto T0_i = std::chrono::steady_clock::now();

    // Initialize main table
    for(i = i0; i <= i1; i++)
	table[i] = i;

    barr->bsync(TID);

    // End init timing here
    auto T1_i = std::chrono::steady_clock::now();
    std::chrono::duration<double> t_walltime_i = T1_i-T0_i;
    double walltime_i = t_walltime_i.count();

    int64 start, stop, size;
    barr->bsync(TID);

    // Begin update timing here
    auto T0 = std::chrono::steady_clock::now();
    
    Block(TID, NT, nupdate, start, stop, size);

    uint64 ran = startr(start);
    const int64 tabszm1 = tabsize-1;
    for (i=0; i<size; i++) {
            ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
	    table[ran & tabszm1] ^= ran;
    }

    barr->bsync(TID);
    // End update timed section
    auto T1 = std::chrono::steady_clock::now();
    std::chrono::duration<double> t_walltime = T1-T0;
    double walltime = t_walltime.count();

    /* Verification of results (in parallel).  */
    uint64 temp = 0x1;
    for (i=0; i<nupdate; i++) {
        temp = (temp << 1) ^ (((int64) temp < 0) ? POLY : 0);
        const uint64 offs = temp & tabszm1;
        uint64 * const p = & table[offs];
        if ((offs >= i0) && (offs <= i1))
            *p ^= temp;     
    }
	
    for (i = i0, nerrors=0; i <= i1; i ++)
        nerrors += (table[i] != i);
 
    barr->bsync(TID);

    /* Save error count in table.  (We can re-use table.)  */
    table[TID] = nerrors;

    barr->bsync(TID);
	
    if (TID == 0) {
      for (i = 1; i < NT; ++i)
           nerrors += table[i];
    // Print timing results
    reportResults(walltime_i, walltime, nupdate, ltabsize, nerrors,thresh,0, NT);
    }
}

// Report results. Called only from thread 0

void reportResults(double walltime_i, double walltime, int64 nupdate, int ltabsize, uint64 totalErrors, double thresh, int vlen, int NT){
    // Print timing results
    double mups = (double) nupdate * 1e-6 / walltime;
    int64 tabsize = ((int64) 1) << (int64) ltabsize;
    double rate = (double) totalErrors/(double) tabsize;
    printf ("init(%.4f s.) up(%.4f s.) rate= %.4f Mup/s\n", walltime_i, walltime, mups);
    char name[] = "C++11";
    printf ("Found %lld errors in %lld locations (%.3f%%, %s).\n",
            (long long) totalErrors, (long long) tabsize, 100.0*rate,
            ((rate <= thresh) ? "passed" : "failed"));
    printf("@ %d, %s, %d, %.4f, %.4f, %.4f, %.3f%%, %s, %lld, %d\n",
          ltabsize, name, NT, mups, walltime, walltime_i,
          100*rate, ((rate <= thresh) ? "P" : "F"), (long long) totalErrors, 0);

    return;
}

//
//  main routine
//
int main(int argc, char *argv[])
{

// Default values

// Log of the size of main table (suggested: half of global memory)
    int l_tabsize = 25;
// Multiplier for # updates to table (suggested: 4x number of table entries)
    int m_update = 4;
    int NT   =  1;                     // # threads
    int Vlen = 0;                      // Aggregation size - 0 for sharedmem
    int dummy = 0;                     // Frequency parameter not used

//  Correctness threshold
    double thresh = 0.1;

    cmdLine(argc, argv, l_tabsize, m_update, NT, Vlen, dummy, thresh);

    int ltabsize = l_tabsize;
    const int64 tabsize = ((int64) 1) << (int64) l_tabsize;
    const int64 nupdate = ((int64) m_update) * tabsize;

    table = new uint64[tabsize];
    
    // Print parameters for run
    printf("nThreads = %d\n", NT);
    printf("Main table size = 2^%d = ", ltabsize);
    //
    // Formatting of 64 bit types: https://en.cppreference.com/w/cpp/types/integer
    printf("%" PRId64 " words\n", tabsize);
    printf("Number of updates = %" PRId64 "\n", nupdate);

    thread *thrds = new thread[NT];
    barr = new CombiningTreeBarrier(NT);
 
    // Fork threads
    for(int t=0; t<NT; t++){
       thrds[t] = thread(update_table,t,NT,Vlen,ltabsize, tabsize, nupdate,thresh);
    }
 
    // Join threads
    for(int t=0;t<NT;t++){
        thrds[t].join();
    }

    delete [] thrds;

    return(0);
}
