#ifndef _guppie_hpp
#define _guppie_hpp

#include <inttypes.h>

// Types used by program (should be 64 bits)
// #define uint64 uint64_t
// #define int64 int64_t
typedef uint64_t uint64;
typedef int64_t int64;


// Random number generator
const uint64 POLY = 0x0000000000000007UL;
const int64 PERIOD = 1317624576693539401L;
#endif
