#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>

void cmdLine(int argc, char *argv[], int& l_tabsize, int& m_update, int& NT, int& Vlen, int& freq, double& thresh){
   static struct option long_options[] = {
        {"l_tabsize", required_argument, NULL, 'l'},  // Log of table size
        {"m_update", required_argument, NULL, 'm'},  // Multiplier
                                                // # updates = M *table size
        {"NT", required_argument, NULL, 't'},         // # threads
        {"Vlen", required_argument, NULL, 'v'},       // # vector length
        {"Freq", required_argument, NULL, 'f'},       // # progress frequency 
        {"Thresh", required_argument, NULL, 'h'},     // correctness threshold %
   };
   // Parse the command line arguments -- *if something is not kosher, die
   for(int ac=1;ac<argc;ac++) {
       int c;
       while ((c=getopt_long(argc,argv,"l:m:t:v:f:h:?",long_options,NULL)) != -1){
        switch (c) {

            case 'l':
                l_tabsize = atoi(optarg);
                break;

            case 'm':
                m_update = atoi(optarg);
                break;

            case 't':
                NT = atoi(optarg);
                break;

            case 'v':
                Vlen = atoi(optarg);
                break;

            case 'f':
                freq = atoi(optarg);
                break;

            case 'h':
                thresh = atof(optarg);
                break;


	    // Error
            case '?':
//	       if (!myrank)
//		   std::cerr << "Error in command line argument: " << optarg << std::endl;
               exit(-1);
	       break;

            default:  // You can't get here !!
	       break;
          }
      }
  }
}
