// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//

#define __STDC_FORMAT_MACROS 1
#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <upcxx/upcxx.hpp>

#include "cmdLine.cpp"
#include "guppie.hpp"
#include "guppie-upcxx.hpp"
#include "OrderlyOutput.cpp"

#if UPCXX_VERSION < 20180900
#error This test requires UPC++ 2018.9.0 or newer (reduce, upcxx command)
#endif

using namespace std;
using namespace upcxx;

void reportResults(double walltime_i, double walltime, int64 nupdate, int ltabsize, uint64 totalErrors, double thresh, int vlen){
    if ( !rank_me() ) {
    // Print timing results
    double mups = (double) nupdate * 1e-6 / walltime;
    int64 tabsize = ((int64) 1) << (int64) ltabsize;
    double rate = (double) totalErrors/(double) tabsize;
    printf ("init(%.4f s.) up(%.4f s.) rate= %.4f Mup/s\n", walltime_i, walltime, mups);
    intrank_t nranks = upcxx::rank_n();
    printf ("Found %lld errors in %lld locations (%.3f%%, %s).\n",
            (long long) totalErrors, (long long) tabsize, 100.0*rate,
            ((rate <= thresh) ? "passed" : "failed"));
    printf("@ %d, %s, %d, %.4f, %.4f, %.4f, %.3f%%, %s, %lld, %d\n", 
          ltabsize, alg_name, (int) nranks, mups, walltime, walltime_i, 
          100*rate, ((rate <= thresh) ? "P" : "F"), (long long) totalErrors, vlen);
    return;
    }
}
//
//  main routine
//
int main(int argc, char *argv[])
{

// Default values

   upcxx::init();
   intrank_t myrank = upcxx::rank_me();
   intrank_t nranks = upcxx::rank_n();

// Log of the size of main table (suggested: half of global memory)
    int l_tabsize = 25;
// Multiplier for # updates to table (suggested: 4x number of table entries)
    int m_update = 4;
// Batching size
    int Vlen     = 1024;
//  Frequency (must divide Vlen and be a power of 2)
    int freq     = 4;

//  Correctness threshold
//  NOTE: benchmark spec requires less than 1% loss of updates. 
//  However there are several problems:
//  1) Our validation code actually measures incorrect final table values, which is at best
//     an indirect proxy for lost updates. The latter cannot be reliably determined after-the-fact,
//     as the collision of two update writes to the same table entry may result in a garbage value.
//     With high probability every lost update implies the targeted table entry will 
//     have an incorrect final value, but it's very possible for two or more lost updates to
//     conflate into the same incorrect final table entry, resulting in potential under-reporting 
//     the true number of lost updates.
//  2) The number of updates is (by default) four times the number of table entries,
//     so assuming no conflation of failed updates, the relative error will be over-reported
//     by up to a factor of four.
//  3) The specified 1% error threshold was designed to tolerate random collisions in the 
//     batches of max size 1024 updates permitted by the lookahead restrictions, assuming
//     uniform random distribution of updates in a table size comparable to half of the system's
//     physical memory (another benchmark requirement). However running at smaller table sizes
//     (as this implementation allows) will magnify the collisions and hence the reported
//     error for all batched algorithms.
    double thresh = 0.01;

    int dummy;
    cmdLine(argc, argv, l_tabsize, m_update, dummy, Vlen,freq,thresh);

    #define FATAL_ERR(message) do {                        \
      if (!myrank) cerr << "ERROR: " << message << endl;   \
      upcxx::finalize();                                   \
      return -1;                                           \
    } while (0)

    // freq must be a power of 2
    if ( (freq <= 0) || (!((freq & (freq-1)) == 0)) || ((Vlen > 1) && (Vlen % freq)))
      FATAL_ERR("Frequency [" << freq << "] must be a power of 2 that divides the batching length [" << Vlen << "]");

    if (Vlen > MAX_VLEN) FATAL_ERR("Vlen [" << Vlen << "] may not exceed MAX_VLEN [" << MAX_VLEN << "]");

    int ltabsize = l_tabsize;
    const int64 tabsize = ((int64) 1) << (int64) l_tabsize;
    const int64 nupdate = ((int64) m_update) * tabsize;
    if ( (nranks <= 0) || (!((nranks & (nranks-1)) == 0)) || (tabsize % nranks)){
        if (!myrank)
            cerr << "Illegal number of ranks.\n # ranks must be a power of 2 that divides the global table size evenly." << endl;
        upcxx::finalize();
        return(-1);
    }
    if (nupdate % Vlen) FATAL_ERR("Vlen [" << Vlen << "] must evenly divide nupdate [" << nupdate << "]");
    
    // Print parameters for run
    if (!myrank){
        #if UPCXX_ASSERT_ENABLED
          printf("WARNING: UPCXX_CODEMODE=debug, performance results should not be trusted!\n");
        #endif
        double slop = 8;
        double expected_error = (slop * Vlen * nranks * (nupdate/tabsize)) / tabsize;
        if (alg_error_free) { // these are error-free by design
          expected_error = 0;
        }
        if (thresh < expected_error) { // heuristic warning: see points above
          printf("WARNING: Error threshold %.3f%% may be too tight for the given job parameters. Consider increasing the table size.\n",thresh*100);
        }
        printf("nThreads = %d\n", nranks);
        printf("Main table size = 2^%d = ", ltabsize);
        // Formatting of 64 bit types: https://en.cppreference.com/w/cpp/types/integer
        printf("%" PRId64 " words\n", tabsize);
        printf("Number of updates = %" PRId64 "\n", nupdate);
        printf("Expected error <= %.3f%%\n", 100*expected_error);
    }
    upcxx::barrier();

    update_table(myrank,nranks,Vlen,ltabsize, tabsize, nupdate, freq, thresh);

    upcxx::finalize();
 

    return(0);
}
