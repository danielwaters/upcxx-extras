#ifndef _guppie_upcxx_hpp
#define _guppie_upcxx_hpp

#ifndef MAX_VLEN
#define MAX_VLEN 1024
#endif

extern void reportResults(double walltime_i, double walltime, 
                          int64 nupdate, int ltabsize, 
			  uint64 totalErrors, double thresh, int vlen);

extern void update_table(int myrank, int nranks, int Vlen,
                  int ltabsize, int64 tabsize, int64 nupdate, 
		  int freq, double thresh);

extern const char *alg_name; // algorithm name
extern int alg_error_free;   // is it error-free?

#endif
