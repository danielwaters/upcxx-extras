// Solves Laplace's equation using relaxation method.
// Processes only expose their boundaries via distributed objects.
// upcxx -O lap2D.cpp -o lap2D
// upcxx-run -np <nprocs> ./lap2D <max_iters> <npts_x> [ <npts_y> <np_x> <np_y> ]

#include <chrono>
#include <cmath>
#include <iostream>
#include "upcxx/upcxx.hpp"

int nrows, ncols, received = 0;

enum {West, East, South, North};

// Arrays of global pointers
upcxx::global_ptr<double> ghosts[4], neighbors[4];

// Global boundary conditions
void set_bc(int ranks[4], double *xlocal)
{
    if (ranks[West] < 0)
        for (int i = 0; i < nrows; ++i)
            ghosts[West].local()[i] = xlocal[i * ncols] = 1.0;

    if (ranks[East] < 0)
        for (int i = 0; i < nrows; ++i)
            ghosts[East].local()[i] = xlocal[i * ncols + ncols - 1] = 1.0;

    if (ranks[South] < 0)
        for (int i = 0; i < ncols; ++i)
            ghosts[South].local()[i] = xlocal[(nrows - 1) * ncols + i] = 1.0;

    if (ranks[North] < 0)
        for (int i = 0; i < ncols; ++i)
            ghosts[North].local()[i] = xlocal[i] = 1.0;
}

void exchange_halo(double *xlocal)
{
    constexpr ptrdiff_t elem_sz = (ptrdiff_t)sizeof(double);

    // Strided signaling-put for west boundary (column)
    if (neighbors[West] != nullptr)
        upcxx::rput_strided<2>(
            xlocal, {{elem_sz, ncols * elem_sz}},
            neighbors[West], {{elem_sz, elem_sz}},
            {{1, (unsigned int)nrows}}, upcxx::remote_cx::as_rpc([](){ ++received; }));

    // Strided signaling-put for east boundary (column)
    if (neighbors[East] != nullptr)
        upcxx::rput_strided<2>(
            &xlocal[ncols - 1], {{elem_sz, ncols * elem_sz}},
            neighbors[East], {{elem_sz, elem_sz}},
            {{1, (unsigned int)nrows}}, upcxx::remote_cx::as_rpc([](){ ++received; }));

    // Contiguous signaling-put for south boundary (row)
    if (neighbors[South] != nullptr)
        rput(&xlocal[(nrows - 1) * ncols], neighbors[South], ncols, upcxx::remote_cx::as_rpc([](){ ++received; }));

    // Contiguous signaling-put for north boundary (row)
    if (neighbors[North] != nullptr)
        rput(xlocal, neighbors[North], ncols, upcxx::remote_cx::as_rpc([](){ ++received; }));
}

// Relaxation method for the inner domain
inline double compute_inner(double *xlocal, double *xnew)
{
    double diff = 0.0;
    for (int i = 1; i < nrows - 1; ++i)
    {
        for (int j = 1; j < ncols - 1; ++j)
        {
            int idx = i * ncols + j;

            xnew[idx] = 0.25 * (xlocal[idx + ncols] +
                                      xlocal[idx - ncols] +
                                      xlocal[idx + 1] +
                                      xlocal[idx - 1]);

            diff += (xnew[idx] - xlocal[idx]) * (xnew[idx] - xlocal[idx]);
        }
    }

    return diff;
}

// Relaxation method for the boundaries
inline double compute_bdry(double *xlocal, double *xnew)
{
    xnew[0] = 0.25 * (ghosts[West].local()[0] + ghosts[North].local()[0] + xlocal[1] + xlocal[ncols]);
    double diff = (xnew[0] - xlocal[0]) * (xnew[0] - xlocal[0]);

    for (int i = 1; i < ncols - 1; ++i)
    {
        xnew[i] = 0.25 * (xlocal[i - 1] + ghosts[North].local()[i] + xlocal[i + 1] + xlocal[i + ncols]);
        diff += (xnew[i] - xlocal[i]) * (xnew[i] - xlocal[i]);
    }

    xnew[ncols - 1] = 0.25 * (xlocal[ncols - 2] + ghosts[North].local()[ncols - 1] + ghosts[East].local()[0] + xlocal[ncols - 1 + ncols]);
    diff += (xnew[ncols - 1] - xlocal[ncols - 1]) * (xnew[ncols - 1] - xlocal[ncols - 1]);

    for (int i = 1; i < nrows - 1; ++i)
    {
        int idx0 = i * ncols;
        xnew[idx0] = 0.25 * (ghosts[West].local()[i] + xlocal[idx0 - ncols] + xlocal[idx0 + 1] + xlocal[idx0 + ncols]);
        diff += (xnew[idx0] - xlocal[idx0]) * (xnew[idx0] - xlocal[idx0]);

        idx0 += ncols - 1;
        xnew[idx0] = 0.25 * (xlocal[idx0 - 1] + ghosts[East].local()[i] + xlocal[idx0 + ncols] + xlocal[idx0 - ncols]);
        diff += (xnew[idx0] - xlocal[idx0]) * (xnew[idx0] - xlocal[idx0]);
    }

    int idx = nrows * ncols - ncols;
    xnew[idx] = 0.25 * (ghosts[West].local()[nrows - 1] + xlocal[idx - ncols] + xlocal[idx + 1] + ghosts[South].local()[0]);
    diff += (xnew[idx] - xlocal[idx]) * (xnew[idx] - xlocal[idx]);

    for (int i = 1; i < ncols - 1; ++i)
    {
        xnew[idx + i] = 0.25 * (xlocal[idx + i - ncols] + xlocal[idx + i - 1] + xlocal[idx + i + 1] + ghosts[South].local()[i]);
        diff += (xnew[idx + i] - xlocal[idx + i]) * (xnew[idx + i] - xlocal[idx + i]);
    }

    idx += ncols - 1;
    xnew[idx] = 0.25 * (xlocal[idx - 1] + ghosts[South].local()[ncols - 1] + ghosts[East].local()[nrows - 1] + xlocal[idx - ncols]);
    diff += (xnew[idx] - xlocal[idx]) * (xnew[idx] - xlocal[idx]);

    return diff;
}

int main(int argc, char *argv[])
{
    int my_rank, nprocs, npts_x, npts_y, np_x, np_y, max_iters, count = 0, nbrs = 0, ranks[4];
    double *xlocal, *xnew, diff_norm, gdiff_norm = 0.0, tolerance = 0.01;
    std::chrono::time_point<std::chrono::steady_clock> tic, toc;

    upcxx::init();

    my_rank = upcxx::rank_me();
    nprocs = upcxx::rank_n();

    if ((argc < 3 || argc > 6) && !my_rank)
    {
        std::cerr << "Usage: lap2D <max_iters> <npts_x> [ <npts_y> <np_x> <np_y> ]\n";
        exit(1);
    }
    upcxx::barrier();

    // Get command-line arguments
    max_iters = atoi(argv[1]);
    npts_x = atoi(argv[2]);

    if (argc > 3)
        npts_y = atoi(argv[3]);
    else
        npts_y = npts_x;

    if (argc > 4)
        np_x = atoi(argv[4]);
    else
    {
        np_x = sqrt(nprocs);
        while (nprocs % np_x != 0)
            ++np_x;
    }

    if (argc > 5)
        np_y = atoi(argv[5]);
    else
        np_y = nprocs / np_x;

    // Validate arguments
    if (nprocs != np_x * np_y && !my_rank)
    {
        std::cerr << "Number of processes not equal to number of subdomains!\n";
        exit(1);
    }
    if (((npts_x % np_x) || (npts_y % np_y)) && !my_rank)
    {
        std::cerr << "Number of points not evenly divisible by number of subdomains!\n";
        exit(1);
    }
    upcxx::barrier();

    nrows = npts_y / np_y; // Height of subdomain
    ncols = npts_x / np_x; // Width of subdomain

    // Allocate memory in the private heap for my piece of the domain, and set xlocal = 0.0
    xlocal = new double[nrows * ncols]();
    xnew = new double[nrows * ncols];

    // Who are my neighbors?
    ranks[West] = my_rank < np_y ? -1 : my_rank - np_y;
    ranks[East] = my_rank >= nprocs - np_y ? -1 : my_rank + np_y;
    ranks[South] = my_rank % np_y ? my_rank - 1 : -1;
    ranks[North] = (my_rank + 1) % np_y ? my_rank + 1 : -1;

    // Arrays allocated in the shared heap
    ghosts[West] = upcxx::new_array<double>(nrows);
    ghosts[East] = upcxx::new_array<double>(nrows);
    ghosts[South] = upcxx::new_array<double>(ncols);
    ghosts[North] = upcxx::new_array<double>(ncols);

    {
        // Distributed objects to receive data from adjacent processes
        upcxx::dist_object<upcxx::global_ptr<double>> dist_west {ghosts[West]};
        upcxx::dist_object<upcxx::global_ptr<double>> dist_east {ghosts[East]};
        upcxx::dist_object<upcxx::global_ptr<double>> dist_south {ghosts[South]};
        upcxx::dist_object<upcxx::global_ptr<double>> dist_north {ghosts[North]};

        // Fetch neighbors' distributed objects
        if (ranks[West] >= 0)
        {
            neighbors[West] = dist_east.fetch(ranks[West]).wait();
            ++nbrs;
        }
        if (ranks[East] >= 0)
        {
            neighbors[East] = dist_west.fetch(ranks[East]).wait();
            ++nbrs;
        }
        if (ranks[South] >= 0)
        {
            neighbors[South] = dist_north.fetch(ranks[South]).wait();
            ++nbrs;
        }
        if (ranks[North] >= 0)
        {
            neighbors[North] = dist_south.fetch(ranks[North]).wait();
            ++nbrs;
        }

        upcxx::barrier();
    }

    // Set boundary conditions
    set_bc(ranks, xlocal);

    upcxx::barrier(); // For timing purposes

    if (!my_rank)
    {
        std::cout << "Computing with " << nprocs << " processes...\n";
        std::cout << "npts_x: " << npts_x << '\n';
        std::cout << "npts_y: " << npts_y << '\n';
        std::cout << "np_x: " << np_x << '\n';
        std::cout << "np_y: " << np_y << '\n';
        std::cout << "max_iters: " << max_iters << '\n';
        tic = std::chrono::steady_clock::now();
    }

    while (count < max_iters)
    {
        // Send my boundaries to adjacent processes
        exchange_halo(xlocal);

        diff_norm = compute_inner(xlocal, xnew);

        // Do not proceed till I have received all boundaries
        while (received < nbrs)
            upcxx::progress(); // Make user-level progress (allow incoming RPCs to be processed)

        // Reset received for the next iteration
        received = 0;

        diff_norm += compute_bdry(xlocal, xnew);

        // Collective to perform sum reduction
        gdiff_norm = upcxx::reduce_all(diff_norm, upcxx::op_fast_add).wait(); // Implicit barrier

        gdiff_norm = sqrt(gdiff_norm);

        ++count;

        // Check for convergence
        if (gdiff_norm < tolerance)
            break;

        std::swap(xlocal, xnew); // O(1)
    }

    upcxx::barrier(); // For timing purposes

    if (!my_rank)
    {
        toc = std::chrono::steady_clock::now();
        std::cout << "At iteration #" << count << ", norm is " << gdiff_norm << '\n';
        std::cout << "Elapsed time = " << std::chrono::duration_cast<std::chrono::milliseconds>(toc - tic).count() << " ms\n";

        // Check for known solution
        if (max_iters == 100 && npts_x == 4096 && npts_y == 4096)
        {
            if (fabs(gdiff_norm - 0.878125) < tolerance)
                std::cout << "SUCCESS!\n";
            else
                std::cout << "FAIL!\n";
        }
    }

    // Deallocate memory
    delete[] xlocal;
    delete[] xnew;
    upcxx::delete_array(ghosts[West]);
    upcxx::delete_array(ghosts[East]);
    upcxx::delete_array(ghosts[South]);
    upcxx::delete_array(ghosts[North]);

    upcxx::finalize();

    return 0;
}

