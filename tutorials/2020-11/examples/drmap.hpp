// Distributed hash table with insert and find operations.
// Represented as a distributed object of std::unordered_map's.
// Polymorphic over key and value types, but requires value type 
// to be TriviallySerializable (effectively, TriviallyCopyable).
// Values are assumed to be large, and thus are manipulated by
// pointer and communicated using RMA to eliminate in-memory copies.

#include <map>
#include <cassert>
#include <upcxx/upcxx.hpp>

template<typename key_t, typename val_t>
class DistrRMAMap {
#if UPCXX_SPEC_VERSION >= 20200300
  static_assert(upcxx::is_serializable<key_t>::value, 
                "DistrRMAMap key type must be Serializable");
  static_assert(upcxx::is_trivially_serializable<val_t>::value, 
                "DistrRMAMap value type must be TriviallySerializable");
#endif
protected:
  struct entry_t {
    upcxx::global_ptr<val_t> ptr;
    size_t count;
  };
  // store the local unordered map in a distributed object 
  using dobj_map_t =
    upcxx::dist_object<std::unordered_map<key_t, entry_t>>;

  dobj_map_t local_map{{}};

  // map the key to a target process
  int get_target_rank(const key_t &key) {
    return std::hash<key_t>{}(key) % upcxx::rank_n();
  }

public:
  // implicit default constructor must be called collectively, since
  // it invokes the collective dist_object constructor

  //------------------------------------------------------------------------------------
  // insert a key-value pair into the hash table
  // value may be an array of val_t of size `count`
  #if INSERT_CHECKS
  // This version returns true iff insertion was successful
  // and false otherwise (key already exists in the map)
  upcxx::future<bool> insert(const key_t &key,
                             const val_t *val_ptr, size_t count) {
    upcxx::future<upcxx::global_ptr<val_t>> f = upcxx::rpc(get_target_rank(key),
       // lambda to fetch a landing zone for value
       [](dobj_map_t &lmap, const key_t &key, size_t count) -> upcxx::global_ptr<val_t> {
          // runs at target:
          if (lmap->count(key) > 0) return nullptr; // key collision
          upcxx::global_ptr<val_t> dest_ptr = upcxx::allocate<val_t>(count);
          assert(dest_ptr && "Out of memory!");
          // insert into the local map at the target
          (*lmap)[key] = entry_t{dest_ptr, count};
          return dest_ptr;
       }, local_map, key, count);
    // back at initiator: schedule the RMA put of value and completion indication
    return f.then([=](upcxx::global_ptr<val_t> dest_ptr) -> upcxx::future<bool> {
      // dest_ptr is result from rpc
      if (!dest_ptr) return upcxx::make_future(false); // key collision
      return rput(val_ptr, dest_ptr, count).then([]() { return true; });
    });
  }
  #else
  // This version simply asserts a lack of key collision
  upcxx::future<bool> insert(const key_t &key,
                             const val_t *val_ptr, size_t count) {
    upcxx::future<upcxx::global_ptr<val_t>> f = upcxx::rpc(get_target_rank(key),
       // lambda to fetch a landing zone for value
       [](dobj_map_t &lmap, const key_t &key, size_t count) { // runs at target:
          assert(lmap->count(key) == 0 && "attempt to insert a duplicate key!");
          upcxx::global_ptr<val_t> dest_ptr = upcxx::allocate<val_t>(count);
          assert(dest_ptr && "Out of memory!");
          // insert into the local map at the target
          (*lmap)[key] = entry_t{dest_ptr, count};
          return dest_ptr;
       }, local_map, key, count);
    // back at initiator: schedule the RMA put of value and completion indication
    return f.then([=](upcxx::global_ptr<val_t> dest_ptr) { // dest_ptr is result from rpc
      return rput(val_ptr, dest_ptr, count).then([]() { return true; });
    });
  }
  #endif

  //------------------------------------------------------------------------------------
  // lookup a key in the map
  // if the key is not found, returns future<nullptr,0>
  // otherwise, returns future<ptr,cnt>, where ptr is a pointer to a
  // newly allocated buffer in private memory containing the value array (cnt elements)
  upcxx::future<val_t *,size_t> find(const key_t &key) {
    int tgt_rank = get_target_rank(key);
    upcxx::future<entry_t> f = upcxx::rpc(tgt_rank,
       // lambda to fetch the metadata for the entry from the local map at target
       [](dobj_map_t &lmap, const key_t &key) -> entry_t { // runs at target:
          if (lmap->count(key) == 0) return entry_t{nullptr, 0}; // not found
          else return (*lmap)[key];
       }, local_map, key);
    // back at initiator: schedule the RMA get of value and completion indication
    return f.then([=](const entry_t &e) -> upcxx::future<val_t *,size_t> {
      // e is the entry returned by the rpc from target
      size_t count = e.count;
      if (!e.ptr) return upcxx::make_future((val_t *)nullptr, (size_t)0); // not found
      // check things look sane:
      assert(e.ptr && e.ptr.where() == tgt_rank); 
      assert(e.count > 0 && e.count < 10*1024*1024);
      val_t *dest_ptr = new val_t[count]; // space for result
      return rget(e.ptr, dest_ptr, count) // fetch data
            .then([=]{ return upcxx::make_future(dest_ptr, count); });
    });
  }
  //------------------------------------------------------------------------------------
};
