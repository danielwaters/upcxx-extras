// Pi with a distributed object.
// There is no communication overlap in this implementation.

#include <iostream>
#include <random>
#include <upcxx/upcxx.hpp>

using namespace std;
using namespace upcxx;

// Throws a random dart and returns 1 if it is in the unit circle, 0
// otherwise.
static int hit();

static const int DEFAULT_NUM_TRIALS = 1000000;

int main(int argc, char *argv[]) {
  upcxx::init();

  // determine number of trials for each process
  int trials = argc > 1 ? stoi(argv[1]) : DEFAULT_NUM_TRIALS;
  int my_trials = trials / rank_n();
  if (rank_me() < trials % rank_n()) {
    ++my_trials;
  }

  // perform the trials, storing the number of hits in the dist_object
  dist_object<int> all_hits(0);
  for (int i = 0; i < my_trials; ++i) {
    // dereference obtains this process's representative
    *all_hits += hit();
  }

  // wait for everyone to complete their trials
  barrier();

  // process 0 retrieves each result synchronously
  if (rank_me() == 0) {
    int total = 0;
    for (int i = 0; i < rank_n(); ++i) {
      // fetch obtains another process's representative
      total += all_hits.fetch(i).wait();
    }
    cout << "PI estimated to " << 4.0 * total / trials << endl;
  }

  // barrier needed before destruction of dist_object, but finalize()
  // includes a barrier
  upcxx::finalize();
}

static int hit() {
  // C++11 random number generator
  static default_random_engine generator;
  static uniform_real_distribution<> dist(0.0, 1.0);

  double x = dist(generator);
  double y = dist(generator);
  // determine if the point is in the unit circle
  if (x * x + y * y <= 1.0) {
    return 1;
  } else {
    return 0;
  }
}
