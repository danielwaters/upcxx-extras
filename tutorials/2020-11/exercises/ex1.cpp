// Hello world example with writes to a file.
// Each process independently writes to the file, with no
// synchronization. The writes are race conditions, and the results
// are undefined.

#include <iostream>
#include <fstream>
#include <unistd.h> // for sync
#include <upcxx/upcxx.hpp>

int main() {
  // setup UPC++ runtime
  upcxx::init();
  // open a file for writing
  std::ofstream fout("output.txt", std::ios_base::app);
  fout << "Hello from process " << upcxx::rank_me()
       << " out of " << upcxx::rank_n() << std::endl;
  // commit data to disk
  sync();
  // close down UPC++ runtime
  upcxx::finalize();
  // file will close due to RAII
}
