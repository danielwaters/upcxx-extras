// Hello world example with writes to a file.
// The processes take turns writing to the file.
// On some systems, calls to sync may be necessary after each write to
// flush the data to disk.

#include <iostream>
#include <fstream>
#include <unistd.h> // for sync
#include <upcxx/upcxx.hpp>

int main() {
  // setup UPC++ runtime
  upcxx::init();
  for (int i = 0; i < upcxx::rank_n(); ++i) {
    if (upcxx::rank_me() == i) {
      // open the file when it's my turn
      std::ofstream fout("output.txt", std::ios_base::app);
      fout << "Hello from process " << upcxx::rank_me()
           << " out of " << upcxx::rank_n() << std::endl;
      // commit data to disk
      sync();
      // file will close due to RAII
    }
    // barrier prevents anyone from proceeding until everyone is here
    upcxx::barrier();
  }
  // close down UPC++ runtime
  upcxx::finalize();
}
