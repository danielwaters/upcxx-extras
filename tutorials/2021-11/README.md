# 2021-11 UPC++ Tutorial at SC21

This directory contains various materials from the UPC++ tutorial
presented at [SC21](https://sc21.supercomputing.org/presentation/?id=tut113&sess=sess215) in November 2021.

The [tutorial slides are here](https://upcxx.lbl.gov/wiki/pubs/SC21-UPCXX-tutorial-slides.pdf).

You can [download this entire repo](https://bitbucket.org/berkeleylab/upcxx-extras/get/master.zip)
or clone with a command like:
```bash
git clone https://bitbucket.org/berkeleylab/upcxx-extras.git
```
in either case, these tutorial materials are located in the tutorials/2021-11 directory.

Except where otherwise noted, all files in this directory are 
authored by the LBNL Pagoda group, and subject to the copyright notice 
and licensing terms in the top-level [LICENSE.txt](../../LICENSE.txt).

The UPC++ example codes have been tested with  UPC++ 1.0 v2021.9.0
which is available [here](https://upcxx.lbl.gov).

The [upcxx.lbl.gov](https://upcxx.lbl.gov) site also includes links to API
documentation, downloads and publications.

Online resources targeted at this tutorial's attendees have been collected
[here](https://go.lbl.gov/sc21).  This includes a "Preparation" section with
links to instructions for use of public installs of UPC++ at various computing
centers, and multiple options for installation and use on your own laptop or
workstation.

## Generic usage instructions

Assuming you have UPC++ installed in your `$PATH`, you can 
build and run code in the `examples` or `exercises` directory with a command like the following:
```bash
make all run
```
A single example or exercise can be compiled and run with a `run-[PROG]` convenience target:
```bash
make run-hello-world
```
If UPC++ is installed elsewhere (e.g. `/usr/local/upcxx`) you can 
use a command like the following:
```bash
make all run UPCXX_INSTALL=/usr/local/upcxx
```
If you want to adjust compile flags, you can use a command like:
```bash
make clean all EXTRA_FLAGS='-O -network=udp'
```

Please note:  
If using one of the provided
[public installs](https://bitbucket.org/berkeleylab/upcxx/wiki/docs/site-docs)
of UPC++ at
NERSC, OLCF or ALCF, then execution (`make run` or `upcxx-run`) *must* take
place within a resource allocation (interactive or batch).  The same may also
be true of your home institution's cluster.  However, instructions for use of
the batch schedulers used on these systems are beyond the scope of this
tutorial.

### Acknowledgments:

This UPC++ Tutorial is developed and maintained by the 
[Pagoda Project](https://go.lbl.gov/pagoda) in the 
[CLaSS Group](https://go.lbl.gov/class) 
at Lawrence Berkeley National Laboratory (LBNL), and is funded primarily by the 
[Exascale Computing Project (ECP)](https://www.exascaleproject.org/research-project/pagoda-upc-gasnet/)
(17-SC-20-SC), a collaborative effort of the U.S. Department of Energy Office
of Science and the National Nuclear Security Administration.

Versions of this tutorial have been presented at the International Conference for
High Performance Computing, Networking, Storage, and Analysis (SC20, SC21)
funded by ACM and IEEE, and other venues under ECP funding.

All tutorial materials are subject to 
[these license terms](https://bitbucket.org/berkeleylab/upcxx-extras/src/master/LICENSE.txt).

